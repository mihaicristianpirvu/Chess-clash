#include <Chess/Knight.hpp>

constexpr KnightTable Knight::knightTable;

Knight::Knight(Color color) : Piece(color) { 
	this->symbol = (color == Color::White) ? 'N' : 'n';
	this->type = "Knight";
}

bool Knight::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> positions) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;

	if (oldI == newI || oldJ == newJ || (abs(oldI-newI) + abs(oldJ-newJ) != 3))
		return false;
	return true;
}

Knight::~Knight() { }
