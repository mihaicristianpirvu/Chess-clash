#include <Chess/Bishop.hpp>

constexpr BishopTable Bishop::bishopTable;

Bishop::Bishop(Color color) : Piece(color) { 
	this->symbol = (color == Color::White) ? 'B' : 'b';
	this->type = "Bishop";
}

bool Bishop::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> positions) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;

	/* The indices difference remain constant, so we check if this difference is changed first. */
	if(abs(oldI-newI) != abs(oldJ-newJ))
		return false;

	/* But the difference is not enough, so we must go on the respective diagonal from oldI towards newI,
	 * and if there is a piece on the way, we return false. Otherwise the move was legal. We don't need to check
	 * if the position at the end is free or opponent's piece, becuase this check is done for all moves at start. */
	int signI = (oldI < newI) ? 1 : -1;
	int signJ = (oldJ < newJ) ? 1 : -1;
	int i = oldI + signI, j = oldJ + signJ;
	while(i < newI) {
		if(board[i][j]->getType() != "NoPiece")
			return false;
		i += signI;
		j += signJ;
	}

	return true;
}

Bishop::~Bishop() { }
