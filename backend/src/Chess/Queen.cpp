#include <Chess/Queen.hpp>

Queen::Queen(Color color) : Piece(color) {
	this->symbol = (color == Color::White) ? 'Q' : 'q';
	this->type = "Queen";
}

bool Queen::isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &board, int moveNumber,
	std::tuple<int, int, int, int> positions) {
	int oldI, oldJ, newI, newJ;
	std::tie(oldI, oldJ, newI, newJ) = positions;

	Bishop bishop(this->color);
	Rook rook(this->color);
	return bishop.isValidMove(board, moveNumber, positions) || rook.isValidMove(board, moveNumber, positions);
}

Queen::~Queen() { }
