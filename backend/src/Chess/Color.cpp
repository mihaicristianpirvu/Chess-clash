#include <Chess/Color.hpp>

constexpr int Color::White;
constexpr int Color::Black;
constexpr int Color::None;

Color::Color() : value(Color::None) {}
Color::Color(int value) : value(value) {}

Color Color::getColor(int i) {
	if(i == Color::White)
		return Color::White;
	if(i == Color::Black)
		return Color::Black;

	return Color::None;
}

Color Color::getOppositeColor(const Color &color) {
	if(color == Color::None)
		return Color::None;

	return color == Color::White ? Color::Black : Color::White;
}

Color Color::getOppositeColor(int i) {
	return getOppositeColor(Color::getColor(i));
}

Color Color::getColor() {
	return Color::getColor(this->value);
}

int Color::getValue(const Color &color) {
	return color.getValue();
}

int Color::getValue() const {
	return this->value;
}

bool Color::operator==(const int value) const {
	return value == this->getValue();
}

bool Color::operator!=(const int value) const {
	return !(value == this->getValue());
}

bool Color::operator==(const Color &color) const {
	return color.getValue() == this->getValue();
}

bool Color::operator!=(const Color &color) const {
	return !(color == this->getValue());
}

std::string Color::toString() const{
	return this->value == Color::White ? "White" : "Black";
}