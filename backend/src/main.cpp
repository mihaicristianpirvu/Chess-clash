#include <ArgParser.hpp>
#include <ChessGame.hpp>
#include <iostream>
#include <string>


int main(int argc, char **argv) {
	ArgParser parser(argc, argv);
	parser.setUsage("Usage: ./chess-clash <config=PATH> [logfile=PATH]");
	parser.addArgument(ValuedArgument<std::string>("config", "Path to config file"), ArgType::MANDATORY);
	parser.addArgument(ValuedArgument<std::string>("logfile", "Path to log file. If none is set, the log will be "
		"printed to stdout"));
	parser.parseArguments();

	auto cfgFile = parser.getArgument<ValuedArgument<std::string>>("config")->getValue();
	std::string logFile = !parser.isSet("logfile") ? "" :
		parser.getArgument<ValuedArgument<std::string>>("logfile")->getValue();
	
	ChessGame chessGame(cfgFile, logFile);
	chessGame.startChessProcesses();
	chessGame.playGame();

	return 0;
}