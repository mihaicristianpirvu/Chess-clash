/* King.h - Header for a king piece */
#ifndef KING_H
#define KING_H

#include <Chess/Piece.hpp>

struct KingTable {
	static constexpr int index(int i, int j, int k) {
		return i * 8 * 9 + j * 9 + k;
	}

	constexpr KingTable() : values()
	{
		constexpr int directions[3] = {-1, 0, 1};
		for (auto i=0; i<8; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;
				for(auto direction_i : directions) {
					for(auto direction_j : directions) {
						if(direction_i == 0 && direction_j == 0)
							continue;
						if(i+direction_i>=0 && i+direction_i<=7 && j+direction_j>=0 && j+direction_j<=7) {
							values[index(i, j, p)] = Pair<int, int>(i+direction_i, j+direction_j);
							++p;
						}
					}
				}
				values[index(i,j,p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	Pair<int, int> values[8*8*9];
};

class King : public Piece {
public:
	King(Color color);
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int, 
	std::tuple<int, int, int, int>);	
	~King();

	constexpr static KingTable kingTable = KingTable();
	bool canCastle;
	bool lastMoveCouldCastle;
};

#endif // KING_H