/* Piece.h - Interface for a basic chess piece */
#ifndef PIECE_H
#define PIECE_H

#include <Chess/Color.hpp>
#include <ctype.h>
#include <memory>
#include <string>
#include <tuple>
#include <Utilities/Pair.hpp>
#include <vector>

class Piece {
public:
	Piece() {}
	Piece(Color color) : color(color) {}
	virtual char toChar();
	virtual std::string &getType();
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int,
		std::tuple<int, int, int, int>);
	Color getColor();
	virtual ~Piece() {}

	int lastMoved = -1;

protected:
	Color color;
	char symbol = 0;
	std::string type;
};

#endif /* PIECE_H */