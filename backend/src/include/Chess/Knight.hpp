/* Knight.h - Header for a knight piece */
#ifndef KNIGHT_H
#define KNIGHT_H

#include <Chess/Piece.hpp>

/* Statically generate all knight positions for check check */
struct KnightTable {
	static constexpr int index(int i, int j, int k) {
		return i * 8 * 9 + j * 9 + k;
	}

	constexpr KnightTable() : values()
	{
		constexpr int indices[8][2]={{1, 2}, {1, -2}, {-1, 2}, {-1, -2}, {2, 1}, {2, -1}, {-2, 1}, {-2, -1}};
		for (auto i=0; i<8; ++i) {
			for(auto j=0; j<8; ++j) {
				auto p = 0;
				for(auto k=0; k<8; ++k) {
					if(i+indices[k][0]<=7 && i+indices[k][0]>=0 && j+indices[k][1]<=7 && j+indices[k][1]>=0) {
						values[index(i, j, p)] = Pair<int, int>(i+indices[k][0], j+indices[k][1]);
						++p;
					}
				}
				values[index(i,j,p)] = Pair<int, int>(-1, -1);
			}
		}
	}
	Pair<int, int> values[8*8*9];
};

class Knight : public Piece {
public:
	Knight(Color);
	virtual bool isValidMove(const std::vector<std::vector<std::shared_ptr<Piece>>> &, int, 
		std::tuple<int, int, int, int>);
	~Knight();

	constexpr static KnightTable knightTable = KnightTable();
};

#endif // KNIGHT_H