#ifndef COLOR_H
#define COLOR_H

#include <string>

class Color {
public:
	static Color getColor(int);
	static Color getOppositeColor(const Color &);
	static Color getOppositeColor(int);
	static int getValue(const Color &);

	Color();
	Color(int);
	bool operator==(const int) const;
	bool operator!=(const int) const;
	bool operator==(const Color &) const;
	bool operator!=(const Color &) const;
	Color getColor();
	int getValue() const;
	std::string toString() const;

	static constexpr int White = 0;
	static constexpr int Black = 1;
	static constexpr int None = 2;
private:
	int value;
};

#endif /* COLOR_H */