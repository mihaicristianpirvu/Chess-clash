/* NoPiece.h - A no-piece on a chess board */
#ifndef NO_PIECE_H
#define NO_PIECE_H

#include <Chess/Piece.hpp>
#include <memory>

class NoPiece : public Piece {
public:
	NoPiece();
	/* Returns another shared_ptr to the noPiece */
	static std::shared_ptr<NoPiece> get();
	~NoPiece();
private:
	/* There's only one noPiece and multiple pointers to it in the board. */
	static std::shared_ptr<NoPiece> noPiece;
};

#endif // NO_PIECE_H