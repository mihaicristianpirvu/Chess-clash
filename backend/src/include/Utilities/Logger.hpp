#ifndef LOGGER_HPP
#define LOGGER_HPP

#include <fstream>
#include <iostream>
#include <memory>
#include <string>

class Logger {
public:
	Logger(const std::string &file = "") :
		ptr(file.empty() ? nullptr : std::make_unique<std::ofstream>(file, std::ios_base::out | std::ios_base::trunc)),
		out(file.empty() ? std::cout : *ptr) {
	}

	template<typename T> 
	Logger &operator<<(T x) {
		out << x;
		out.flush();
		return *this;
	}

private:
	std::unique_ptr<std::ostream> ptr;
	std::ostream &out;
};

#endif /* LOGGER_HPP */