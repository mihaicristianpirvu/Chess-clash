/* Debug.h -- Implementation of a basic debug class */
#ifndef DEBUG_H
#define DEBUG_H

#include <string>
#include <iostream>

/* Define all the debug flags here */
class DebugFlag {
public:
	static constexpr int FlagTrue = 1;
	static constexpr int FlagFalse = 0;
	static constexpr int PrintSpawning = 0;
	static constexpr int PrintIniSettingsFile =0;
	static constexpr int PrintGameSettings = 0;
	static constexpr int PrintChildMessages = 0;
	static constexpr int PrintReadyChessProcess = 0;
	static constexpr int PrintGameEachTurn = 1;
	static constexpr int EndGameStatus = 1;
	static constexpr int PrintUndo = 0;
	static constexpr int PrintHistoryMapFlag = 0;
	static constexpr int PrintPlayerTimer = 0;
};

template <bool T>
class Debug {
public:
	template <typename U>
	Debug &operator<<(U) {
		return *this;
	}

	static Debug &get() {
		return writer;
	}

	static Debug<T> writer;
};

template <>
class Debug<true> {
public:

	template<typename U> 
    Debug &operator<<(U x)
    {
        out << x;
        return *this;
    }

	static Debug &get() {
		return writer;
	}

private:
    std::ostream &out = std::cout;
    static Debug<true> writer;
};

template <bool T>
Debug<T> Debug<T>::writer;

#endif /* DEBUG_H */