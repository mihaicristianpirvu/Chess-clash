/* ChessGame.h - Class for a Chess game between 2 Piped Processes */
#ifndef CHESS_PROCESS_H
#define CHESS_PROCESS_H

#include <bitset>
#include <Chess/Bishop.hpp>
#include <Chess/Color.hpp>
#include <Chess/King.hpp>
#include <Chess/Knight.hpp>
#include <Chess/NoPiece.hpp>
#include <Chess/Pawn.hpp>
#include <Chess/Queen.hpp>
#include <Chess/Rook.hpp>
#include <chrono>
#include <IniReader.hpp>
#include <map>
#include <memory>
#include <PipedProcess/PipedProcess.hpp>
#include <stdexcept>
#include <unordered_set>
#include <Utilities/Debug.hpp>
#include <Utilities/Logger.hpp>
#include <string>
#include <vector>


class ChessGame {
public:
	ChessGame(std::string="settings.ini", std::string="");
	void playGame(); /* Plays a chess game between the 2 chess processes */
	void startChessProcesses(); /* Starts the 2 chess processes, after the arguments were parsed accordingly. */
	std::string toString() const; /* Method that prints the current state of the game, including the chess board */
	~ChessGame();

	/* Communication Protocol used by the Chess processes to start a chess game. */
	struct CommunicationProtocol {
		public:
		/* Generates a Key_1=Value_1|Key_2=Value_2|...|Key_n=Value_n string based on the settings map. */
		static std::string toString(bool flipTeams=false);

		constexpr static const char *initMessage = "INIT";
		constexpr static const char *readyMessage = "READY";
		constexpr static const char *goMessage = "GO";
		constexpr static const char *invalidMoveMessage = "INVALID";
		constexpr static const char *winMessage = "WIN";
		constexpr static const char *lossMessage = "LOSS";
		constexpr static const char *drawMessage = "DRAW";
		constexpr static const char *drawRequestMessage = "DRAW-REQUEST";

		/* This is the settings map. Based on this variable, the settings string for both chess processes is created
		 * using the Key=Value form. Default values are also provided, but if they are defined in the config file,
		 * they will be replaced. */
		static std::map<std::string, int> settings;
		const static std::vector<std::string> definedSettings;
	};

private:
	struct Player;
	void buildTable(); /* Method called to instantiate the chess pieces */
	/* This will call the IniReader object, ready the config file and build the settings map that is sent to childs */
	void parseSettings();
	void parseProcessSettings(const IniReader &);
	void startChessGame();

	/** Methods for the Chess Game **/
	/* Returns a tuple containing true and 4 ints if the move is correctly constructed, or false and 4 irelevant ints
	 * otherwise. The move must always be of form 'a2a4' where the first 2 chars represent the original position of the
	 * piece and the next 2 chars represent the destination. The method will return false only if the string is 
	 * malformed or out of the board, not if the move itself is illegal. This check is done in the move() method. */
	std::tuple<bool, std::tuple<int, int, int, int, char>> parseMove(const std::string &);
	/* Makes the move itself, and returns true if the move is legal and false if the move is illegal. If the move is
	 * legal, the move will be also sent to the opponent of this player and if the move is illegal, it will send 
	 * a lose message to the current player and a win message to his opponent and then the process will end. */
	bool move(const std::tuple<int, int, int, int, char> &, struct ChessGame::Player &);
	bool canCastle(const std::tuple<int, int, int, int> &, const struct ChessGame::Player &) const;
	/* The rvalue isInCheck will check for any piece if that position is in check, while the const lvalue one will
	 * check if the given position of the player is a king */
	bool isInCheck(const struct ChessGame::Player &) const;
	bool isInCheckType(const struct ChessGame::Player &, const std::shared_ptr<Piece> &, const std::string &) const;
	/* Check if there's any piece between these 2 positions, be it liniarry or diagonally */
	bool isPieceBetween(int, int, int, int) const;
	/* Checks that have to be done before an move. */
	bool preMoveChecks(const std::tuple<int, int, int, int> &, const struct ChessGame::Player &) const;
	/* Actions that should be done before the actual move, like moving a king or opponent's piece. */
	bool preMoveActions(const std::tuple<int, int, int, int> &, struct ChessGame::Player &);
	/* Actions that should be done after an actual move, such as pawn promotion, or moving the rook after castle */
	bool postMoveActions(const std::tuple<int, int, int, int, char> &, struct ChessGame::Player &);
	/* Returns true if the player is in a mate position (any legal move will leave the king in check) */
	bool isMatePosition(struct ChessGame::Player &);
	/* Undoes the last move, based on the saved information in the modified pieces map. It must undo any pawn
	 * promotion, en passant and castle to the state that was before the move. It also decreases the lastMoved 
	 * variable to the previous value. */
	void undoMove(const std::tuple<int, int, int, int> &);
	void drawRequest(const std::string &, int);
	bool movesRepetition(const struct ChessGame::Player &, const struct ChessGame::Player &) ;
	void exitGame(int);

	struct ArrayBitSetComparator {
		/* Important!!! This must return true if a1 is less than a2 becuase c++ */
		template <size_t N, size_t M>
		bool operator() (const std::array<std::bitset<M>, N> &a1, const std::array<std::bitset<M>, N> &a2) const {
			if(M > 64)
				throw 1;

			for(size_t i=0; i<N; ++i) {
				if(a1[i].to_ullong() < a2[i].to_ullong())
					return true;
				else if(a1[i].to_ullong() > a2[i].to_ullong())
					return false;
				else
					continue;
			}
			return false;
		}
	};

	static const int numProcesses = 2;
	static const int boardSize = 8;
	static const int bufferSize = 1024;
	static const int numPieces = 6;
	static std::map<std::string, int> piecesBitMapOrder;
	constexpr static const char *defaultSettingsFile = "settings.ini";

	PipedProcess process[numProcesses];
	std::vector<std::vector<std::shared_ptr<Piece>>> board;
	std::string configFile;
	/* Stores the modified pieces information */
	std::vector<std::tuple<int, int, std::shared_ptr<Piece>>> modifiedPieces;
	int mainMoveLastMoved;
	/* Stores the maps that have been so far for 3 moves repetition */
	std::map<std::array<std::bitset<64>, 12>, int, ArrayBitSetComparator> historyMap;
	/* Logger that saves the game and players' moves */
	Logger logger;

	/** Variables for the Chess Game **/
	struct Player {
		Player() {}
		Player(const Color &color, const std::tuple<int, int> &kingPosition) : color(color),
			kingPosition(kingPosition), piecesBitMap{0,0,0,0,0,0} {}

		/* Removes a piece of a given position from the list of pieces as well as the bitset! */
		void removePiece(const std::tuple<int, int> &, const std::string &);
		void addPiece(const std::tuple<int, int> &, const std::string &);

		/* Safe only if each item in the tuple is at most 16 bits (and they are, only 3 bits are used for positions) */
		struct tupleintint_hasher {
			std::size_t operator() (const std::tuple<int, int> &key) const {
				return std::hash<int>()((std::get<0>(key)<<16) ^ std::get<1>(key));
			}
		};

		/* The amount of thie this player has */
		std::chrono::milliseconds playerTimer;
		std::unique_ptr<struct Player> opponent;
		Color color;
		std::tuple<int, int> kingPosition;
		std::unordered_set<std::tuple<int, int>, tupleintint_hasher> piecesPosition;
		std::array<std::bitset<64>, ChessGame::numPieces> piecesBitMap;
		std::vector<std::string> moves;
	};

	int moveNumber = 0;
	Player player[numProcesses];
};

#endif /* CHESS_PROCESS_H */